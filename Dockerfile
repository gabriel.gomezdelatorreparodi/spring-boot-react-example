FROM gcr.io/google-appengine/openjdk:8
LABEL maintainer="gabriel.gomezdelatorre@hotmail.com"
# setting microservice user
ENV USER=microservice
ENV UID=10500
ENV GID=10600
ENV TZ=America/Lima
# install timezone
RUN echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y tzdata curl telnet && \
    rm /etc/localtime && \
    ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure -f noninteractive tzdata
RUN addgroup --gid "$GID" "$USER" \
    && adduser \
    --disabled-password \
    --gecos "" \
    --ingroup "$USER" \
    --uid "$UID" \
    "$USER"
RUN apt-get clean
# setting pre-build directories
RUN mkdir -p /apps/config && \
    mkdir -p /var/log/us && \
    chown -R $UID:$GID /apps && \
    chown -R $UID:$GID /var/log/us
# setting application configuration

COPY entrypoint.sh /apps/entrypoint.sh
RUN chmod +x /apps/entrypoint.sh
WORKDIR /apps
EXPOSE 8080
COPY target/*.jar /apps/application.jar
USER $USER
CMD ["sh", "entrypoint.sh"]
